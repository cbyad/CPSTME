package filesPrio.contract;

import filesPrio.service.Filesprio;

public class FilesPrioContract<T> extends FilesPrioDecorator<T> {

	public FilesPrioContract(Filesprio<T> service) {
		super(service);
	}
	
	public void checkInvariant() {
		/* Invariants */
		
		// \inv: getSize() == \sum_{ i \in getActivePrios() } getSizePrios(i)
		
		// \inv: isEmpty() == (getSize()==0)
		// \inv: isActive(i)==getActivePrios.contains(i)
		// \inv : getMaxPrio()==max( getActivePrios() )
		// \inv : getPrio(i)== getElemPrio(i,1)
		// \inv : getElem()==getPrio( getMaxPrio()) 
		// \inv : \forall i:int && getActivePrios().contains(i) , getSizePrio(i)>0
		// \inv : \forall i:int &&   !getActivePrios().contains(i) getSizePrio(i)==0
		// \inv : \forall i:int && getActivePrios().contains(i) , \forall k:int && k>=1 && k<=getSizePrio(i),getElemPrio(i,k)!=null
		
		
	}
	
	public static void checkSwitchInvariant(Filesprio serv) {
		new FilesPrioContract(serv).checkInvariant();
	}
	
	@Override
	public void init() {
		// PRE : pas de précondition
		
		// Traitement
		super.init();
		
		// post-init invariant
		checkInvariant();
		
		// post: isOn() == false
		if(!(isOn() == false))
			throw new PostconditionError("isOn() == false");
		
		// post: getCount() == 0
		if(!(getCount() == 0))
			throw new PostconditionError("getCount() == 0");

	}
	
	@Override
	public void press() {
		
		// pre: isWorking()
		if(!(isWorking()))
			throw new PreconditionError("isWorking()");

		// pre-invariant
		checkInvariant();

		// Captures
		boolean isOn_at_pre = isOn();
		int getCount_at_pre = getCount();
		
		// TRAITEMENT
		super.press();

		// post-invariant
		checkInvariant();

		// post: isOn() == !(isOn()@pre)
		if(!(isOn() == !(isOn_at_pre)))
			throw new PostconditionError("isOn() == !(isOn()@pre");
		
		// post: getCount() == getCount()@pre + 1
		if(!(getCount() == getCount_at_pre + 1))
			throw new PostconditionError("getCount() == getCount()@pre + 1");

	}

}
