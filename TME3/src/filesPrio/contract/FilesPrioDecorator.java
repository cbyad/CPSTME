package filesPrio.contract;

import java.util.Set;

import filesPrio.service.Filesprio;

public  abstract class FilesPrioDecorator<T> implements Filesprio<T> {
	
	private Filesprio<T> delegate;
	
	protected FilesPrioDecorator(Filesprio<T> delegate) {
		this.delegate = delegate;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return delegate.getSize();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return delegate.isEmpty();
	}

	@Override
	public Set<Integer> getActivePrios() {
		// TODO Auto-generated method stub
		return delegate.getActivePrios();
	}

	@Override
	public boolean isActive(int index) {
		// TODO Auto-generated method stub
		return delegate.isActive(index);
	}

	@Override
	public int getMaxPrio() {
		// TODO Auto-generated method stub
		return delegate.getMaxPrio();
	}

	@Override
	public int getSizePrio(int index) {
		// TODO Auto-generated method stub
		return delegate.getSizePrio(index);
	}

	@Override
	public T getPrio(int index) {
		// TODO Auto-generated method stub
		return delegate.getPrio(index);
	}

	@Override
	public T getElem() {
		// TODO Auto-generated method stub
		return delegate.getElem();
	}

	@Override
	public T getElemPrio(int index1, int index2) {
		// TODO Auto-generated method stub
		return delegate.getElemPrio(index1, index2);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		delegate.init();
	}

	@Override
	public void putPrio(int index, T val) {
		// TODO Auto-generated method stub
		delegate.putPrio(index, val);// inv: isOff() == !isOn()
		if(!(isOff() == !isOn()))
			throw new InvariantError("isOff() == !isOn()");
		
		// inv: getCount() >= 0
		if(!(getCount() >= 0))
			throw new InvariantError("getCount() >= 0");

	}

	@Override
	public void put(T val) {
		// TODO Auto-generated method stub
		delegate.put(val);
	}

	@Override
	public void removePrio(int index) {
		// TODO Auto-generated method stub
		delegate.removePrio(index);
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		delegate.remove();
	}
	
	

}
