package filesPrio.service;

import java.util.Set;

public interface Filesprio<T> {

	/* Observators */
	
	public int getSize();
	public boolean isEmpty();
	public Set<Integer> getActivePrios(); 
	public boolean isActive(int index);
	public  int getMaxPrio();
	public int getSizePrio(int index);

	// \pre : getSizePrio(index)>0
	public T getPrio(int index);
	
	// \pre: getSize() >0
	public T getElem();
	
	// \pre: getActivePrios().contains(index1) && 0<index2 && index2<= getSizePrio(index1)
	public T getElemPrio(int index1 , int index2);
	
	
	/* Invariants */
	
	// \inv: getSize() == \sum_{ i \in getActivePrios() } getSizePrios(i)
	// \inv: isEmpty() == (getSize()==0)
	// \inv: isActive(i)==getActivePrios.contains(i)
	// \inv : getMaxPrio()==max( getActivePrios() )
	// \inv : getPrio(i)== getElemPrio(i,1)
	// \inv : getElem()==getPrio( getMaxPrio()) 
	// \inv : \forall i:int && getActivePrios().contains(i) , getSizePrio(i)>0
	// \inv : \forall i:int &&   !getActivePrios().contains(i) getSizePrio(i)==0
	// \inv : \forall i:int && getActivePrios().contains(i) , \forall k:int && k>=1 && k<=getSizePrio(i),getElemPrio(i,k)!=null
	
	
	/* Constructors */

	// \post : getSize()==0 ;
	public void init();
	
	
	/* Operators */
	
	// \pre: index >0 && val!=null
	// \post: !isActive(index)@pre || getActivePrios()==getActivePrios()@pre
	// \post: isActive(index)@pre || getActivePrios()==(getActivePrios()@pre \\union {index}) 
	// \post: getSizePrio(index)==getSizePrio(index)@pre + 1
	// \post: \forall j:int && getActivePrios()@pre.contains(j) && j!=index getSizePrio(j)==getSizePrio(j)@pre
	// \post: getPrio(index)==val||getPrio(index)==getPrio(index)@pre
	// \post: \forall k:int && k>=2 && k<=getSizePrio()@pre+1 , getElemPrio(index,k)==getElemPrio(index,k-1)@pre
	// \post: \forall j:int && getActivePrios()@pre.contains(j) && j!=index , \forall k:int && k>=1 && k<=getSizePrio(j)@pre,
	// getElemPrio(j, k)== getElemPrio(j,k)@pre
	public void putPrio(int index , T val ) ;
	
	// \pre : val!=null
	// \post: put(val)=putPrio(val,getMaxPrio())
	public void put(T val);
	
	// \pre : getSizePrio(index)>0
	// \post: !(getSizePrio(index)@pre>1) || getActivePrios()==getActivePrios()@pre
	// \post: getSizePrio(index)@pre==1 || getActivePrios()==(getActivePrios()@pre.remove(index) ) 
	// \post: getSizePrio(index)==getSizePrio(index)@pre - 1
	// \post: \forall j:int && getActivePrios()@pre.contains(j) && j!=index getSizePrio(j)==getSizePrio(j)@pre
	// \post: \forall k:int && k>=1 && k<=getSizePrio()@pre+1 , getElemPrio(index,k)==getElemPrio(index,k)@pre
	// \post: \forall j:int && getActivePrios()@pre.contains(j) && j!=index , \forall k:int && k>=1 && k<=getSizePrio(j)@pre,
	// getElemPrio(j, k)== getElemPrio(j,k)@pre	
	public void removePrio(int index);
	
	// \pre : getSize(index)>0
	// \post : remove()=removePrio(getMaxPrio)
	public void remove() ;
	
}
