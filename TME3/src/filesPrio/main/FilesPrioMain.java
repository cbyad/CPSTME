package filesPrio.main;

import filesPrio.contract.FilesPrioContract;
import filesPrio.impl.filesPrioImpl;
import filesPrio.service.Filesprio;

public class FilesPrioMain {

	public static void main(String... args) {
		
		System.out.println("Sans contrat :");

		Filesprio s1 = new filesPrioImpl();
		s1.init();
		for(int i=0;i<30;i++) {
			s1.press();
			System.out.println("[" + i + "]: " + s1);
		}

		System.out.println("Avec contrat :");
		
		Filesprio s2 = new filesPrioImpl();
		Filesprio s2Contracted = new FilesPrioContract(s2);
		s2Contracted.init(); // avec contrat
		// s2.init();  // sans contrat
		for(int i=0;i<30;i++) {
			s2Contracted.press(); // avec contrat
			// s2.press();   // sans contrat
			System.out.println("[" + i + "]: " + s2);
		}
		
	}
}
